using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public static UIController instance;
    private Slider healthSlider;
    private Text healthTextValue;
    public GameObject deathScreen;

    private void Awake()
    {
        instance = this;
        Component[] components = GetComponentsInChildren<Component>(true);

        foreach (Component element in components)
        {
            switch (element.name)
            {
                case "HealthSlider":
                    healthSlider = element.GetComponentInChildren<Slider>(true);
                    break;
                case "DeathScreen":
                    deathScreen = element.gameObject;
                    break;
                default:
                    break;
            }

        }
        healthTextValue = healthSlider.GetComponentInChildren<Text>();

    }

    public void SetHealthValue(int value)
    {
        
        healthSlider.value = value;
        healthTextValue.text = value.ToString();
    }

    void Start()
    {
        healthSlider.maxValue = PlayerHealthController.instance.maxHealth;
        healthSlider.value = PlayerHealthController.instance.maxHealth;
        healthTextValue.text = PlayerHealthController.instance.maxHealth.ToString();

    }

    void Update()
    {
        
    }
}
