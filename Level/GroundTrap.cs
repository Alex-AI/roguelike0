using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundTrap : MonoBehaviour
{
    private int damage = 10;
    private float damageRate = 20f;
    private float attackTimer;

    private void Start()
    {
        attackTimer = damageRate;
    }
    private void FixedUpdate()
    {
        if (attackTimer > 0) attackTimer -= Time.deltaTime * 100;
    }

    private void DamagePlayer()
    {
        if (attackTimer < 0)
        {
            attackTimer = damageRate;
            PlayerHealthController.instance.DamagePlayer(damage);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(PlayerController.playerTag))
            DamagePlayer();
    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.CompareTag(PlayerController.playerTag))
            DamagePlayer();
    }

}
