using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenPieces : MonoBehaviour
{
    private float moveSpeed = 3f;
    private Vector3 moveDirection;
    private float deceleration = 5f;
    private float maxLifetime = 10f;
    private float lifetime;

    void Start()
    {
        moveDirection.x = Random.Range(-moveSpeed, moveSpeed);
        moveDirection.y = Random.Range(-moveSpeed, moveSpeed);
        lifetime = Random.Range(1, maxLifetime);
    }

    void FixedUpdate()
    {
        transform.position += moveDirection * moveSpeed * Time.deltaTime;
        moveDirection = Vector3.Lerp(moveDirection, Vector3.zero, deceleration * Time.deltaTime);

        lifetime -= Time.deltaTime;
        if (lifetime <= 0) Destroy(gameObject);
    }
}

