using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Breakable : MonoBehaviour
{
    [SerializeField]
    private GameObject[] brokenPieces;
    private int maxBrokenPieces = 5;

    [SerializeField]
    private int breakableHealth = 20;

    public void DamageBreakable(int damage)
    {
        breakableHealth -= damage;
        if(breakableHealth <= 0)
        {
            DestroyWithBrokenPieces();
        }

    }

    private void DestroyWithBrokenPieces()
    {
        Destroy(gameObject);
        int brokenPiecesCount = Random.Range(1, maxBrokenPieces);
        for (int i = 0; i < brokenPiecesCount; i++)
        {
            int randBrokenPieceIdx = Random.Range(0, brokenPieces.Length - 1);
            Vector3 brokenPieceTransformOffset = new Vector3(Random.Range(0.2f, 0.7f), Random.Range(0.2f, 0.7f), 0);
            Instantiate(brokenPieces[randBrokenPieceIdx], transform.position + brokenPieceTransformOffset, transform.rotation);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == PlayerController.instance.tag && PlayerController.instance.isPlayerDashing())
        {
            DestroyWithBrokenPieces();
        }
    }
}
