using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProjectileController : MonoBehaviour
{
  
    private Vector3 direction;

    /* Hit effect */
    [SerializeField]
    private GameObject hitParticle;

    /* Speed */
    [SerializeField]
    private float speed = 2f;

    /* Damage */
    private int damage = 10;

    void Start()
    {
        direction = PlayerController.instance.transform.position - transform.position;
        direction.Normalize();
    }

    private void FixedUpdate()
    {
        transform.position += direction * speed * Time.deltaTime;
    }

    private void SafeInstantiate(GameObject entity, Vector3 position, Quaternion angle)
    {
        if (entity != null)
            Instantiate(entity, position, angle);
    }

    private void DamageOtherObject(Collider2D collision)
    {
        if (collision.CompareTag(PlayerController.playerTag))
        {
            PlayerHealthController.instance.DamagePlayer(damage);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageOtherObject(collision);
        SafeInstantiate(hitParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
