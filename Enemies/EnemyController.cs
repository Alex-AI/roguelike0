using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Rigidbody2D rbody;

    [SerializeField]
    private float moveSpeed = 2;

    [SerializeField]
    private float agressionRadius = 8;
    [SerializeField]
    private float attackRange = 6;
    [SerializeField]
    private float attackSpeed = 40f; /* msec */
    private static float attackTimer;
    [SerializeField]
    private GameObject projectile;

    private Vector3 moveDirection;

    private Animator animator;

    [SerializeField]
    private int health = 20;

    [SerializeField]
    private GameObject deathParcticle;

    private SpriteRenderer sprite;


    private void Awake()
    {
        rbody = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    public void DamageEnemy(int damage)
    {
        health -= damage;
        if (health <= 0)
        {
            Destroy(gameObject);
            SafeInstantiate(deathParcticle, transform.position, transform.rotation);
        }
    }

    private void PlayAnimation()
    {
        if (PlayerController.instance.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
        }
        else
        {
            transform.localScale = Vector3.one;
        }

        if (moveDirection != Vector3.zero)
            animator.SetBool("isMoving", true);
        else
            animator.SetBool("isMoving", false);
    }

    private void SafeInstantiate(GameObject entity, Vector3 position, Quaternion angle)
    {
        if (entity != null)
            Instantiate(entity, position, angle);
    }

    private void FixedUpdate()
    {

        if(sprite.isVisible && PlayerController.instance.gameObject.activeInHierarchy)
        {
            float distanceToThePlayer = Vector3.Distance(transform.position, PlayerController.instance.transform.position);
            attackTimer -= Time.deltaTime * 100;
            moveDirection = Vector3.zero;

            if (distanceToThePlayer < agressionRadius && distanceToThePlayer > attackRange)
            {
                moveDirection = PlayerController.instance.transform.position - transform.position;
            }
            else if (distanceToThePlayer < agressionRadius && distanceToThePlayer < attackRange)
            {
                if (attackTimer <= 0)
                {
                    attackTimer = attackSpeed;
                    SafeInstantiate(projectile, transform.position, transform.rotation);
                }
            }

            rbody.velocity = moveDirection.normalized * moveSpeed;

            PlayAnimation();
        }

    }
}

