using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public static PlayerController instance;
    public const string playerTag = "Player";
    [SerializeField]
    private float moveSpeed = 6;
    public float activeMoveSpeed;
    [SerializeField]
    private float dashSpeedMultipler = 4f;  
    private bool isDash = false;
    private float dashLenght = 4f, dashTimer = 0; /* msec */
    private float dashCooldown = 100, dashCooldownTimer = 0; /* msec */

    private Vector2 moveInput;
    private Rigidbody2D rbody;
    [SerializeField]
    private Transform gunArm;
    Quaternion gunArmRotation;
    private Camera mainCamera;
    private Animator playerAnim;
    private SpriteRenderer sprite;
    [SerializeField]
    private GameObject bulletToFire;
    [SerializeField]
    private Transform firePoint;
    private float attackSpeed = 20;
    private float attackTimer;
    private bool isOneShot;
    private bool isFiring;

    public bool isPlayerDashing()
    {
        return (dashTimer > 0);
    }
    private void Awake()
    {
        instance = this;
        rbody = GetComponent<Rigidbody2D>();
        mainCamera = Camera.main;
        playerAnim = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    void Update()
    {
        moveInput.x = Input.GetAxisRaw("Horizontal");
        moveInput.y = Input.GetAxisRaw("Vertical");

        if (Input.GetMouseButtonDown(0))
            isOneShot = true;
 
        if (Input.GetMouseButton(0))
            isFiring = true;

        if (Input.GetKeyDown(KeyCode.Space))
            isDash = true;


        Vector3 mousePos = Input.mousePosition;
        Vector3 screenPoint = mainCamera.WorldToScreenPoint(transform.localPosition);
        Vector2 offset = new Vector2(mousePos.x - screenPoint.x, mousePos.y - screenPoint.y);
        float angle = Mathf.Atan2(offset.y, offset.x) * Mathf.Rad2Deg;
        gunArmRotation = Quaternion.Euler(0f, 0f, angle);

        if (mousePos.x < screenPoint.x)
        {
            transform.localScale = new Vector3(-1f, 1f, 1f);
            gunArm.localScale = new Vector3(-1f, -1f, 1f);
        }
        else
        {
            transform.localScale = Vector3.one;
            gunArm.localScale = Vector3.one;
        }

        if (moveInput != Vector2.zero)
            playerAnim.SetBool("isMoving", true);
        else
            playerAnim.SetBool("isMoving", false);

    }


    private void FixedUpdate()
    {
        if (isDash && dashCooldownTimer <= 0)
        {
            
            activeMoveSpeed = moveSpeed * dashSpeedMultipler;
            PlayerHealthController.instance.SetInvincibility();
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, .3f);
            
            playerAnim.SetTrigger("dash");
            if (moveInput == Vector2.zero)
            {
                moveInput = new Vector2(Mathf.Sign(transform.localScale.x), 0);
            }
            dashTimer = dashLenght;
            dashCooldownTimer = dashCooldown;
            
        }

        dashTimer -= (dashTimer > 0) ? Time.deltaTime * 100 : 0;
        dashCooldownTimer -= (dashCooldownTimer > 0) ? Time.deltaTime * 100 : 0;
        if (dashTimer <= 0)
        {
            sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, 1f);
            activeMoveSpeed = moveSpeed;  
        }
        


        rbody.velocity = moveInput.normalized * activeMoveSpeed;
        gunArm.rotation = gunArmRotation;


        if (isOneShot)
        {
            Instantiate(bulletToFire, firePoint.position, firePoint.rotation);
            attackTimer = attackSpeed;
        }

        attackTimer -= (attackTimer > 0) ? Time.deltaTime * 100 : 0;
        if (isFiring)
        {
            if(attackTimer <= 0)
            {
                Instantiate(bulletToFire, firePoint.position, firePoint.rotation);
                attackTimer = attackSpeed;
                
            }
            
        }

        isOneShot = false;
        isFiring = false;
        isDash = false;
    }
}
