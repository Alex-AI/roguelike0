using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBulletController : MonoBehaviour
{
    private float speed = 20f;
    private Rigidbody2D rbody;
    [SerializeField]
    private GameObject hitParticle;
    [SerializeField]
    private int damage = 5;

    private void Awake()
    {
        rbody = GetComponent<Rigidbody2D>(); 
    }
    private void FixedUpdate()
    {
        rbody.velocity = transform.right * speed;
    }

    private void SafeInstantiate(GameObject entity, Vector3 position, Quaternion angle)
    {
        if (entity != null)
            Instantiate(entity, position, angle);
    }

    private void DamageOtherObject(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            collision.GetComponent<EnemyController>().DamageEnemy(damage);
        }
        else if (collision.CompareTag("Breakable"))
        {
            collision.GetComponent<Breakable>().DamageBreakable(damage);
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        DamageOtherObject(collision);
        SafeInstantiate(hitParticle, transform.position, transform.rotation);
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
    
