using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealthController : MonoBehaviour
{
    public static PlayerHealthController instance;
    public int maxHealth = 150;
    public int currentHealth;
    private bool isInvincibility;
    private float invincibilityLenght = 15f, invincibilityTimer = 0;

    public void DamagePlayer(int damage)
    {
        if(invincibilityTimer <= 0)
        {
            currentHealth -= damage;
            UIController.instance.SetHealthValue(currentHealth);
            if (currentHealth <= 0)
            {
                PlayerController.instance.gameObject.SetActive(false);
                UIController.instance.deathScreen.SetActive(true);
            }
        }
       
    }

    public void SetInvincibility()
    {
        invincibilityTimer = invincibilityLenght;
    }
    private void Awake()
    {
        instance = this;
        currentHealth = maxHealth;
    }

    void Start()
    {
    }

    private void FixedUpdate()
    {
        invincibilityTimer -= (invincibilityTimer > 0) ? Time.deltaTime * 100 : 0;
    }
}
